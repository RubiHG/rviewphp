<?php
require_once('phpManager/resenas.php');
require_once('phpManager/pojo/resena.php');
require_once('phpManager/usuarios.php');
require_once('phpManager/pojo/usuario.php');
require_once('phpManager/compartidos.php');
require_once('phpManager/pojo/compartir.php');
require_once('phpManager/pojo/comentario.php');
require_once('phpManager/megusta.php');
require_once('phpManager/pojo/megusta.php');
require_once('phpManager/pojo/perfil.php');
$crudUsuario=new CrudUsuario();
$cuentasPerfil= $crudUsuario->obtenerCantidadSeguidores($_GET["id"]);
$usuario=$crudUsuario->verPerfil($_GET["id"]);
$listaUsuarios=$crudUsuario->obtenerUsuarios();
$crud=new CrudResenas();
$listaResenas=$crud->obtenerTodasResenas($_GET["id"]);
$crudCompartir= new CrudCompartir();
?>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>RView</title>
        <link rel="icon" type="image/png" href="img/Rview.png"/>
        <link type="text/css" href="css/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link type="text/css" href="css/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        
        <link type="text/css" href="css/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
        <link href="css/fontawesome/css/all.min.css" rel="stylesheet" />
        <link href="css/popup.css" rel ="stylesheet"/>
        <link type="text/css" href="css/theme.css" rel="stylesheet">
    </head>
    <body>
    <div class="wrapper">
            <header class="header-top" header-theme="light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between">
                        <div class="top-menu d-flex align-items-center">
                            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                            <div class="header-search row" style="width: 500;">
                                <div class="input-group col-md-11">
                                    <a href=<?php echo "Index.php?id=".$_GET["id"]?> class="btn btn-light ml-10"><i class="fa fa-home"></i>Inicio</a>
                                    <a href=<?php echo "categoria.php?id=1&idPerfil=".$_GET["id"]?> class="btn btn-light ml-10"><i class="far fa-bookmark"></i>Categorias</a>                                    
                                </div>
                                <div class="col-md-1">
                                    <img src="img/LogoRview.png" width="90" height="30">
                                </div>
                            </div>
                        </div>
                        <div class="top-menu d-flex align-items-center">
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="img/user.png" alt=""></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href=<?php echo "perfil.php?id=".$_GET["id"]."&visitado=".$_GET["id"]?>><i class="far fa-user"></i> Perfil</a>
                                    <a class="dropdown-item" href="<?php echo "editarPerfil.php?id=".$_GET["id"]?>"><i class="fa fa-cogs"></i> Editar</a>
                                    <a class="dropdown-item" href="login.php"><i class="fa fa-sign-out-alt"></i> Salir</a>
                                </div>
                            </div>
                            <button type="button" class="btn btn-light ml-10 btnresena" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-star"></i>Reseña</button>
                        </div>
                    </div>
                </div>
            </header>
            <div class="page-wrap">
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-center"> 
                                            <img src="img/user.png" class="rounded-circle" width="150" height="150">
                                            <hr class="mb-0"> 
                                            <h4 class="card-title mt-10"><?php echo $usuario->getUsername()?></h4>
                                            <p class="card-subtitle"><?php echo $usuario->getCorreo()?></p>
                                            <div class="row text-center justify-content-md-center" style="margin-left: -15;">
                                                    <div class="col-4 text-resena"><b>Reseñas<br><?php echo $cuentasPerfil->getResenas()?></b></div>
                                                    <div class="col-4 text-resena"><b >Seguidores <br><?php echo $cuentasPerfil->getSeguidores()?></b></div>
                                                    <div class="col-4 text-resena"><b>Siguiendo <br><?php echo $cuentasPerfil->getSeguidos()?></b></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7">
                                <?php foreach ($listaResenas as $resena) {?>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row ml-5">
                                                <div class="col-7"><b><?php echo $resena->getTitulo() ?></b></div>
                                                <div class="col-5" style="text-align: right;">
                                                    <?php for( $i = 0; $i <= 4; $i++){?>
                                                        <?php if( $resena->getEstrellas()<=$i){?>
                                                            <i class="far fa-star"></i>
                                                        <?php }else{?>
                                                            <i class="fas fa-star"></i>
                                                        <?php }?>
                                                    <?php }?>
                                                </div>
                                                <div class="col-12"><a href=<?php echo "perfil.php?id=".$_GET["id"]."&visitado=".$resena->getIdUsuario()?>><i><?php echo $resena->getCorreoUsuario() ?></i></a></div>
                                                <br>
                                                <br>
                                                <div class="col-12 mb-2">
                                                    <?php echo $resena->getContenido() ?>
                                                <hr style="color:black;">
                                                </div>
                                                <div class="col-lg-4 text-center">
                                                        <?php if($resena->getMeGusta()!=null){?>
                                                            <form action='phpManager/administrar_megusta.php' method='post'>
                                                                <input type="hidden" name='idResena' value=<?php echo $resena->getIdResena() ?>>
                                                                <input type="hidden" name='idUsuario' value=<?php echo $_GET["id"]?>>
                                                                <input type="hidden" name="quitarMegusta" value="quitarMegusta">
                                                                <button class="btnResenaPost" type="submit" value="submit"><i class="fas fa-lg fa-thumbs-up"  style="color:#722f37;"></i></button>
                                                                </form>
                                                        <?php } else{?>
                                                            <form action='phpManager/administrar_megusta.php' method='post'>
                                                                <input type="hidden" name='idResena' value=<?php echo $resena->getIdResena() ?>>
                                                                <input type="hidden" name='idUsuario' value=<?php echo $_GET["id"]?>>
                                                                <input type="hidden" name="darMegusta" value="darMegusta">
                                                                <button class="btnResenaPost" type="submit" value="submit"><i class="far fa-lg fa-thumbs-up" ></i></button>
                                                            </form>
                                                        <?php } ?>
                                                </div>
                                                <div class="col-lg-4 text-center">
                                                <a href=<?php echo "comentarios.php?id=".$resena->getIdResena()."&idPerfil=".$_GET["id"]?>>
                                                <i  class="far fa-lg fa-comment"></i>
                                                </a>
                                                </div>
                                                <div class="col-lg-4 text-center">
                                                <button type="button" class="btnResenaPost" data-toggle="modal" data-target="<?php echo "#compartir".$resena->getIdResena() ?>">
                                                <i class="far fa-lg fa-share-square"></i>
                                                </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content overlay active">
                        <div class=" popup active" id="popup">
                            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                            <h3>Danos tu opinion</h3>
                            <form action='phpManager/administrar_resena.php' method='post'>
                            <input type='hidden' name='idUsuario' value=<?php echo $_GET["id"]?>>
                                <div class="contenedor-inputs">
                                    <div class="wrap-input100 validate-input" data-validate="Titulo requerido">
                                        <input class="input100" type="text" name="titulo">
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Titulo</span>
                                    </div>
                                    <div class="wrap-textarea100 validate-input" data-validate="Contenido requerida">
                                        <textarea class="input100"  name="contenido" rows="6" cols="50" ></textarea>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Contenido</span>
                                    </div>
                                    
                                    <div class="wrap-input100 validate-input" data-validate="Estrellas requerida">
                                        <select class="custom-select input100" id="inputGroupSelect01" name="idCategoria">
                                            <option > </option>
                                            <option value="1">Libro</option>
                                            <option value="2">Película</option>
                                            <option value="3">Serie</option>
                                            <option value="4">Música</option>
                                        </select>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Categoría</span>
                                    </div>
                                    <div class="wrap-input100 validate-input" data-validate="Categoria requerida">
                                        <select class="custom-select input100" id="inputGroupSelect01" name="estrellas">
                                            <option > </option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Estrellas</span>
                                    </div>
                                </div>
                                <input type='hidden' name='publicar' value='publicar'>
                                <div class="col-6 offset-3 btn" >
                                    <button class="buttonperfil perfil-form-btn" style="margin-top: 0px;" type="submit" value="Submit">Publicar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                
                <!-- Modal compartir reseña -->
                <?php foreach ($listaResenas as $resena) {?>
                 <div class="modal fade" id='<?php echo "compartir".$resena->getIdResena() ?>' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content overlay active">
                        <div class=" popup active" id="popup">
                            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                            <h5 style="text-align: center;">¿Compartir con tus seguidores?</h5>
                            <form action='phpManager/administrar_compartir.php' method='post'>   
                            <input type='hidden' name='idUsuario' value=<?php echo $_GET["id"]?>>
                            <input type='hidden' name='idResena' value='<?php echo $resena->getIdResena() ?>'>
                                <div class="tab-pane fade show mt-25" id="seguidos" role="tabpanel" aria-labelledby="seguidos-tab">
                                                <div cass="row">
                                                    <div class="col-12" style="float: left;">
                                                        <div class="card cardPerfil">
                                                            <div class="card-body">
                                                                <div class="row ml-1">
                                                                    <div class="col-2 pl-1 pr-0"><img src="img/user.png" class="rounded-circle" width="50" height="50"></div>
                                                                    <div  class="col-10 row">
                                                                        <div class="col-12">
                                                                            <b><?php echo $resena->getTitulo() ?></b>
                                                                        </div>
                                                                        <div class="col-12">
                                                                            <?php echo $resena->getContenido() ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                <input type='hidden' name='compartir' value='compartir'>
                                <div class="col-6 offset-3 btn">
                                    <button class="buttonperfil perfil-form-btn" style="margin-top: 0px;" type="submit" value="Submit">Compartir</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>     
                </div>
                <?php }?>
                <footer class="footer">
                    <div class="w-100 clearfix">
                        <span class="text-center text-sm-left d-md-inline-block">Copyright © 2018 uv.mx v1.0. Todos los derechos reservados.</span>
                        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Creado por <a href="#" class="text-dark" target="_blank">Rubix, Rola, Kat, Solis, Andrelolelo, Geral</a></span>
                    </div>
                </footer>
            </div>
        </div>
        <script src="scripts/jquery-3.3.1.js"></script>
        <script src="scripts/popper.min.js"></script>
        <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/moment.js"></script>
        <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/main.js"></script>
        <script src="css/theme.css"></script>
    </body>
</head>