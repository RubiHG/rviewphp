<?php
require_once('phpManager/resenas.php');
require_once('phpManager/pojo/resena.php');
require_once('phpManager/usuarios.php');
require_once('phpManager/pojo/usuario.php');
require_once('phpManager/pojo/comentario.php');
require_once('phpManager/compartidos.php');
require_once('phpManager/pojo/compartir.php');
require_once('phpManager/pojo/comentario.php');
require_once('phpManager/megusta.php');
require_once('phpManager/pojo/megusta.php');
$crudUsuario=new CrudUsuario();
$usuario=$crudUsuario->verPerfil($_GET["idPerfil"]);
$crud=new CrudResenas();
$listaResenas=$crud->obtenerResenasPorCategoria($_GET["id"],$_GET["idPerfil"] );
?>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>RView</title>
        <link rel="icon" type="image/png" href="img/Rview.png"/>
        <link type="text/css" href="css/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link type="text/css" href="css/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        
        <link type="text/css" href="css/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
        <link href="css/fontawesome/css/all.min.css" rel="stylesheet" />
        <link href="css/popup.css" rel ="stylesheet"/>
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link href="css/css-registro/main.css" rel="stylesheet" />
    </head>
    <body>
    <div class="wrapper">
            <header class="header-top" header-theme="light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between">
                        <div class="top-menu d-flex align-items-center">
                            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                            <div class="header-search row" style="width: 500;">
                                <div class="input-group col-md-11">
                                    <a href=<?php echo "Index.php?id=".$_GET["idPerfil"]?> class="btn btn-light ml-10"><i class="fa fa-home"></i>Inicio</a>
                                    <a href=<?php echo "categoria.php?id=".$_GET["id"]."&idPerfil=".$_GET["idPerfil"]?> class="btn btn-light ml-10"><i class="far fa-bookmark"></i>Categorias</a>                                    
                                </div>
                                <div class="col-md-1">
                                    <img src="img/LogoRview.png" width="90" height="30">
                                </div>
                            </div>
                        </div>
                        <div class="top-menu d-flex align-items-center">
                            <div class="header-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="fa fa-times"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="fa fa-search"></i></span>
                                </div>
                            </div>
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="img/user.png" alt=""></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href=<?php echo "perfil.php?id=".$_GET["idPerfil"]."&visitado=".$_GET["idPerfil"]?>><i class="far fa-user"></i> Perfil</a>
                                    <a class="dropdown-item" href=<?php echo "perfil.php?id=".$_GET["idPerfil"]."&visitado=".$_GET["idPerfil"]?>><i class="fa fa-cogs"></i> Editar</a>
                                    <a class="dropdown-item" href="login.php"><i class="fa fa-sign-out-alt"></i> Salir</a>
                                </div>
                            </div>
                            <button type="button" class="btn btn-light ml-10 btnresena" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-star"></i>Reseña</button>
                        </div>
                    </div>
                </div>
            </header>
            <div class="page-wrap">
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div id="cardm" class="card">
                                    <div class="card-body">   
                                        <div class="page-wrapper chiller-theme toggled">
                                           <nav id="sidebar" class="sidebar-wrapper">
                                              <div class="sidebar-content">
                                                 <div class="sidebar-menu">
                                                    <ul >
                                                       <li class="header-menu">
                                                         <span class="tex1">Categorías</span>
                                                       </li>
                                                       <li>
                                                          <a href=<?php echo "categoria.php?id=1&idPerfil=".$_GET["idPerfil"]?> >
                                                             <i id="icon"class="fa fa-book"></i>
                                                             <span class="tex">Libros</span>
                                                          </a>
                                                       </li>
                                                       <li>
                                                          <a href=<?php echo "categoria.php?id=2&idPerfil=".$_GET["idPerfil"]?>>
                                                            <i class="fa fa-film"></i>
                                                            <span class="tex">Peliculas</span>
                                                          </a>
                                                      </li>
                                                      <li>
                                                         <a href=<?php echo "categoria.php?id=3&idPerfil=".$_GET["idPerfil"]?>>
                                                            <i class="fa fa-tv"></i>
                                                            <span class="tex">Series</span>
                                                         </a>
                                                      </li>
                                                      <li>
                                                        <a href=<?php echo "categoria.php?id=4&idPerfil=".$_GET["idPerfil"]?>>
                                                          <i class="fa fa-music"></i>
                                                          <span class="tex">Musica</span>
                                                        </a>
                                                      </li>
                                                    </ul>
                                                </div>
                                              </div>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7 mt-4">
                                <?php foreach ($listaResenas as $resena) {?>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row ml-5">
                                                <div class="col-8"><b><?php echo $resena->getTitulo() ?></b></div>
                                                <div class="col-4 " style="text-align: right;">
                                                    <?php for( $i = 0; $i <= 4; $i++){?>
                                                        <?php if( $resena->getEstrellas()<=$i){?>
                                                            <i class="far fa-star"></i>
                                                        <?php }else{?>
                                                            <i class="fas fa-star"></i>
                                                        <?php }?>
                                                    <?php }?>
                                                </div>
                                                <div class="col-12"><a href=<?php echo "perfil.php?id=".$_GET["idPerfil"]."&visitado=".$resena->getIdUsuario()?>><i><?php echo $resena->getCorreoUsuario() ?></i></a></div>
                                                <br>
                                                <br>
                                                <div class="col-12 mb-2">
                                                    <?php echo $resena->getContenido() ?>
                                                <hr style="color:black;">
                                                </div>
                                                <div class="col-lg-4 text-center">
                                                <?php if($resena->getMeGusta()!=null){?>
                                                            <form action='phpManager/administrar_megusta.php' method='post'>
                                                                <input type="hidden" name='idResena' value=<?php echo $resena->getIdResena() ?>>
                                                                <input type="hidden" name='idUsuario' value=<?php echo $_GET["idPerfil"]?>>
                                                                <input type="hidden" name='categoria' value=<?php echo $_GET["id"]?>>
                                                                <input type="hidden" name="quitarMegustaCategoria" value="quitarMegustaCategoria">
                                                                <button class="btnResenaPost" type="submit" value="submit"><i class="fas fa-lg fa-thumbs-up"  style="color:#722f37;"></i></button>
                                                            </form>
                                                        <?php } else{?>
                                                            <form action='phpManager/administrar_megusta.php' method='post'>
                                                                <input type="hidden" name='idResena' value=<?php echo $resena->getIdResena() ?>>
                                                                <input type="hidden" name='idUsuario' value=<?php echo $_GET["idPerfil"]?>>
                                                                <input type="hidden" name='categoria' value=<?php echo $_GET["id"]?>>
                                                                <input type="hidden" name="MegustaCategoria" value="MegustaCategoria">
                                                                <button class="btnResenaPost" type="submit" value="submit"><i class="far fa-lg fa-thumbs-up" ></i></button>
                                                            </form>
                                                        <?php } ?>
                                                </button>
                                                </form>
                                                </div>
                                                <div class="col-lg-4 text-center">
                                                <a href=<?php echo "comentarios.php?id=".$resena->getIdResena()."&idPerfil=".$_GET["idPerfil"]?>>
                                                <i  class="far fa-lg fa-comment"></i></a> 
                                                </div>
                                                <div class="col-lg-4 text-center">
                                                <button type="button" class="btnResenaPost" data-toggle="modal" data-target="<?php echo "#compartir".$resena->getIdResena() ?>">
                                                <i class="far fa-lg fa-share-square"></i>
                                                </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content overlay active">
                        <div class=" popup active" id="popup">
                            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                            <h3>Danos tu opinion</h3>
                            <form action='phpManager/administrar_resena.php' method='post'>
                            <input type='hidden' name='idUsuario' value=<?php echo $_GET["idPerfil"]?>>
                                <div class="contenedor-inputs">
                                    <div class="wrap-input100 validate-input" data-validate="Titulo requerido">
                                        <input class="input100" type="text" name="titulo">
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Titulo</span>
                                    </div>
                                    <div class="wrap-textarea100 validate-input" data-validate="Contenido requerida">
                                        <textarea class="input100"  name="contenido" rows="6" cols="50" ></textarea>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Contenido</span>
                                    </div>
                                    
                                    <div class="wrap-input100 validate-input" data-validate="Estrellas requerida">
                                        <select class="custom-select input100" id="inputGroupSelect01" name="idCategoria">
                                            <option > </option>
                                            <option value="1">Libro</option>
                                            <option value="2">Película</option>
                                            <option value="3">Serie</option>
                                            <option value="4">Música</option>
                                        </select>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Categoría</span>
                                    </div>
                                    <div class="wrap-input100 validate-input" data-validate="Categoria requerida">
                                        <select class="custom-select input100" id="inputGroupSelect01" name="estrellas">
                                            <option > </option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Estrellas</span>
                                    </div>
                                </div>
                                <input type='hidden' name='publicar' value='publicar'>
                                <div class="col-6 offset-3 btn" >
                                    <button class="buttonperfil perfil-form-btn" style="margin-top: -10px;" type="submit" value="Submit">Compartir</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                
                <!-- Modal compartir reseña -->
                <?php foreach ($listaResenas as $resena) {?>
                 <div class="modal fade" id='<?php echo "compartir".$resena->getIdResena() ?>' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content overlay active">
                        <div class=" popup active" id="popup">
                            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                            <h5 style="text-align: center;">¿Compartir con tus seguidores?</h5>
                            <form action='phpManager/administrar_compartir.php' method='post'>   
                            <input type='hidden' name='idUsuario' value=<?php echo $_GET["idPerfil"]?>>
                            <input type='hidden' name='idResena' value='<?php echo $resena->getIdResena() ?>'>
                                <div class="tab-pane fade show mt-25" id="seguidos" role="tabpanel" aria-labelledby="seguidos-tab">
                                                <div cass="row">
                                                    <div class="col-12" style="float: left;">
                                                        <div class="card cardPerfil">
                                                            <div class="card-body">
                                                                <div class="row ml-1">
                                                                    <div class="col-2 pl-1 pr-0"><img src="img/user.png" class="rounded-circle" width="50" height="50"></div>
                                                                    <div  class="col-10 row pl-20 pr-0" style="padding-left: 30px ;">
                                                                        <div class="col-12">
                                                                            <b><?php echo $resena->getTitulo() ?></b>
                                                                        </div>
                                                                        <div class="col-12">
                                                                            <?php echo $resena->getContenido() ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                <input type='hidden' name='compartirResena' value='compartirResena'>
                                <div class="col-6 offset-3 btn" style="padding-top: 0px; border-top-width: 0px;">
                                    <button class="buttonperfil perfil-form-btn" style="margin-top: 0px;" type="submit" value="Submit">Compartir</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>     
                </div>
                <?php }?>
                

                <footer class="footer">
                    <div class="w-100 clearfix">
                        <span class="text-center text-sm-left d-md-inline-block">Copyright © 2018 uv.mx v1.0. Todos los derechos reservados.</span>
                        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Creado por <a href="#" class="text-dark" target="_blank">Rubix, Rola, Kat, Solis, Andrelolelo, Geral</a></span>
                    </div>
                </footer>
            </div>
    </div>
        <script src="scripts/jquery-3.3.1.js"></script>
        <script src="scripts/popper.min.js"></script>
        <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/moment.js"></script>
        <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="css/theme.css"></script>
        <script src="js/main.js"></script>
    </body>
</head>