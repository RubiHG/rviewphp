<?php
	class Usuario{
		private $idUsuario;
		private $username;
		private $contrasena;
		private $correo;
		private $sobremi;
 
		function __construct(){}
 
		public function getUsername(){
		return $this->username;
		}
 
		public function setUsername($username){
			$this->username = $username;
		}
 
		public function getContrasena(){
			return $this->contrasena;
		}

		public function setContrasena($contrasena){
			$this->contrasena = $contrasena;
		}

		public function getCorreo(){
			return $this->correo;
		}

		public function setCorreo($correo){
			$this->correo = $correo;
		}

		public function getSobremi(){
			return $this->sobremi;
		}
 
		public function setSobremi($sobremi){
			$this->sobremi = $sobremi;
		} 
		
		public function getIdUsuario(){
			return $this->idUsuario;
		}
 
		public function setIdUsuario($idUsuario){
			$this->idUsuario = $idUsuario;
		}
	}
?>