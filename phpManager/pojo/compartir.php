<?php
    class Compartir{
        private $idUsuario;
        private $idResena;
        
        function __construct(){}
        
        public function getIdUsuario(){
			return $this->idUsuario;
		}
 
		public function setIdUsuario($idUsuario){
			$this->idUsuario = $idUsuario;
		}
        
        public function getIdResena(){
			return $this->idResena;
		}
 
		public function setIdResena($idResena){
			$this->idResena = $idResena;
		}
    }
?>