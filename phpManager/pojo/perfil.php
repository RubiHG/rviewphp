<?php

class Perfil{
		private $seguidos;
		private $seguidores;
		private $resenas;
 
		function __construct(){}

		public function getSeguidos(){
			return $this->seguidos;
		}

		public function setSeguidos($seguidos){
			$this->seguidos = $seguidos;
		}
		
		public function getSeguidores(){
			return $this->seguidores;
		}

		public function setSeguidores($seguidores){
			$this->seguidores = $seguidores;
        }

        public function getResenas(){
			return $this->resenas;
		}

		public function setResenas($resenas){
			$this->resenas = $resenas;
        }
}
?>