<?php
	class Resena{
		private $idResena;
		private $idCategoria;
		private $idUsuario;
		private $contenido;
        private $estrellas;
		private $titulo;
		private $correoUsuario;
		private $megusta;
 
		function __construct(){}
        
        public function getIdResena(){
			return $this->idResena;
		}

		public function setIdResena($idResena){
			$this->idResena = $idResena;
        }
        
		public function getTitulo(){
		return $this->titulo;
		}
 
		public function setTitulo($titulo){
			$this->titulo = $titulo;
		}
 
		public function getIdCategoria(){
			return $this->idCategoria;
		}

		public function setIdCategoria($idCategoria){
			$this->idCategoria = $idCategoria;
		}

		public function getIdUsuario(){
			return $this->idUsuario;
		}

		public function setIdUsuario($idUsuario){
			$this->idUsuario = $idUsuario;
		}

		public function getContenido(){
			return $this->contenido;
		}
 
		public function setContenido($contenido){
			$this->contenido = $contenido;
		} 
		
		public function getEstrellas(){
			return $this->estrellas;
		}
 
		public function setEstrellas($estrellas){
			$this->estrellas = $estrellas;
		}

		public function getCorreoUsuario(){
			return $this->correoUsuario;
		}
 
		public function setCorreoUsuario($correoUsuario){
			$this->correoUsuario = $correoUsuario;
		}

		public function getMeGusta(){
			return $this->megusta;
		}
 
		public function setMeGusta($megusta){
			$this->megusta = $megusta;
		}
	}
?>