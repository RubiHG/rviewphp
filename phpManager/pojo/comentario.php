<?php
	class Comentario{
		private $idComentario;
		private $idUsuario;
		private $descripcion;
		private $fecha;
		private $correo;
		private $idResena;
		private $username;
 
		function __construct(){}
        
        public function getIdComentario(){
			return $this->idComentario;
		}

		public function setIdComentario($idComentario){
			$this->idComentario = $idComentario;
		}
		
		public function getIdUsuario(){
			return $this->idUsuario;
		}

		public function setIdUsuario($idUsuario){
			$this->idUsuario = $idUsuario;
        }
        
		public function getDescripcion(){
		return $this->descripcion;
		}
 
		public function setDescripcion($descripcion){
			$this->descripcion = $descripcion;
		}
 
		public function getFecha(){
			return $this->fecha;
		}

		public function setFecha($fecha){
			$this->fecha = $fecha;
		}

		public function getCorreo(){
			return $this->correo;
		}

		public function setCorreo($correo){
			$this->correo = $correo;
		}

		public function getIdResena(){
			return $this->idResena;
		}

		public function setIdResena($idResena){
			$this->idResena = $idResena;
		}

		public function getUsername(){
			return $this->username;
		}

		public function setUsername($username){
			$this->username = $username;
		}
	}
?>