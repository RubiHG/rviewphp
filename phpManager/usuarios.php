<?php
include ("conexion.php");
class CrudUsuario{
    // constructor de la clase
    public function __construct(){}

    function registrar($usuario){
        $myUsuario= new Usuario();
        $consulta="INSERT INTO usuarios (username,correo, contrasena) VALUES ('".$usuario->getUsername()."','".$usuario->getCorreo()."','".$usuario->getContrasena()."')";
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        $consulta2="SELECT * FROM usuarios WHERE correo='".$usuario->getCorreo()."' and contrasena='".$usuario->getContrasena()."'";
        $datosLogin=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
        if($fila=mysqli_fetch_array($datosLogin)){
            $myUsuario->setIdUsuario($fila['idUsuario']);
			$myUsuario->setUsername($fila['username']);
			$myUsuario->setContrasena($fila['contrasena']);
			$myUsuario->setCorreo($fila['correo']);
			$myUsuario->setSobremi($fila['sobremi']);
            mysqli_close($GLOBALS['conexion']);
            return $myUsuario;
        }
        mysqli_close($GLOBALS['conexion']);
        return $myUsuario;
    }

    function login($usuario){
        $myUsuario= new Usuario();
        $consulta="SELECT * FROM usuarios WHERE correo='".$usuario->getCorreo()."' and contrasena='".$usuario->getContrasena()."'";
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        if($fila=mysqli_fetch_array($datos)){
            $myUsuario->setIdUsuario($fila['idUsuario']);
			$myUsuario->setUsername($fila['username']);
			$myUsuario->setContrasena($fila['contrasena']);
			$myUsuario->setCorreo($fila['correo']);
			$myUsuario->setSobremi($fila['sobremi']);
            mysqli_close($GLOBALS['conexion']);
            return $myUsuario;
        }
        mysqli_close($GLOBALS['conexion']);

        return $myUsuario;
    }

    function editarUsuario($usuario){
        $consulta="UPDATE usuarios SET username='".$usuario->getUsername()."', contrasena='".$usuario->getContrasena()."', correo='".$usuario->getCorreo()."', sobremi='".$usuario->getSobremi()."' WHERE idUsuario=".$usuario->getIdUsuario();
        mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        mysqli_close($GLOBALS['conexion']);
    }

    function verPerfil($id){
        $myUsuario= new Usuario();
        $consulta="SELECT * FROM usuarios WHERE idUsuario=".$id;
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        while ($fila=mysqli_fetch_array($datos)){
            
            $myUsuario->setIdUsuario($fila['idUsuario']);
			$myUsuario->setUsername($fila['username']);
			$myUsuario->setContrasena($fila['contrasena']);
			$myUsuario->setCorreo($fila['correo']);
            $myUsuario->setSobremi($fila['sobremi']);
            
        }
        return $myUsuario;
    }

    function eliminarUsuario($usuario){
        $consulta="DELETE FROM usuarios WHERE idUsuario=".$_GET['idUsuario'];
        mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        mysqli_close($GLOBALS['conexion']);
    }


    function obtenerSeguidores($id){
        $arraySeguidores = [];
        $consulta="SELECT seguidores.idUsuario, usuarios.username, usuarios.correo, usuarios.sobremi FROM usuarios INNER JOIN seguidores ON seguidores.idUsuarioSeguido=".$id." and usuarios.idUsuario=seguidores.idUsuario";
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        while ($fila=mysqli_fetch_array($datos)){
            $myUsuario= new Usuario();
            $myUsuario->setIdUsuario($fila['idUsuario']);
			$myUsuario->setUsername($fila['username']);
			$myUsuario->setCorreo($fila['correo']);
            $myUsuario->setSobremi($fila['sobremi']);
            $arraySeguidores[]=$myUsuario;
        }
        return $arraySeguidores;
    }
    
    function obtenerSeguidos($id){
        $arraySeguidores = [];
        $consulta="SELECT seguidores.idUsuarioSeguido, usuarios.username, usuarios.correo, usuarios.sobremi FROM usuarios INNER JOIN seguidores ON seguidores.idUsuario=".$id." and usuarios.idUsuario=seguidores.idUsuarioSeguido";
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        while ($fila=mysqli_fetch_array($datos)){
            $myUsuario= new Usuario();
            $myUsuario->setIdUsuario($fila['idUsuarioSeguido']);
			$myUsuario->setUsername($fila['username']);
			$myUsuario->setCorreo($fila['correo']);
            $myUsuario->setSobremi($fila['sobremi']);
            $arraySeguidores[]=$myUsuario;
        }
        return $arraySeguidores;
    }

    function seguirUsuario($idUsuario, $idUsuarioSeguido){
        $consulta="INSERT INTO seguidores(idUsuario, idUsuarioSeguido) VALUES ('".$idUsuario."','".$idUsuarioSeguido."')";
        $datos= mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        mysqli_close($GLOBALS['conexion']);
        return $datos;
    }

    function dejarDeSeguir($idUsuario, $idUsuarioSeguido){
        $consulta="DELETE FROM seguidores WHERE idUsuario=".$idUsuario." AND idUsuarioSeguido=".$idUsuarioSeguido;
        $datos= mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        mysqli_close($GLOBALS['conexion']);
        return $datos;
    }

    function seguido($idUsuario, $idUsuarioSeguido){
        $consulta="SELECT  COUNT(*)  FROM seguidores WHERE idUsuario=".$idUsuario." and idUsuarioSeguido=".$idUsuarioSeguido;
        $datos= mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        if($fila=mysqli_fetch_array($datos)){
            return $fila['COUNT(*)'];
        }
    }

    function obtenerCantidadSeguidores($idUsuario){
        $myPerfil= new Perfil();
        $consulta2="SELECT  COUNT(*)  FROM seguidores WHERE idUsuario=".$idUsuario;
        $consulta="SELECT  COUNT(*)  FROM seguidores WHERE idUsuarioSeguido=".$idUsuario;
        $consulta3="SELECT  COUNT(*)  FROM resenas WHERE idUsuario=".$idUsuario;
        $datos= mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        $datos2= mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
        $datos3= mysqli_query($GLOBALS['conexion'],$consulta3) or die (mysqli_error($GLOBALS['conexion']));
        while ($fila=mysqli_fetch_array($datos)){
            $myPerfil->setSeguidores($fila['COUNT(*)']);
        }
        while ($fila=mysqli_fetch_array($datos2)){
            $myPerfil->setSeguidos($fila['COUNT(*)']);
        }
        while ($fila=mysqli_fetch_array($datos3)){
            $myPerfil->setResenas($fila['COUNT(*)']);
        }
        return $myPerfil;
    }
    function obtenerUsuarios(){
        $arrayUsuarios = [];
        $consulta="SELECT * from usuarios";
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        while ($fila=mysqli_fetch_array($datos)){
            $myUsuario= new Usuario();
            $myUsuario->setIdUsuario($fila['idUsuario']);
			$myUsuario->setUsername($fila['username']);
			$myUsuario->setCorreo($fila['correo']);
            $myUsuario->setSobremi($fila['sobremi']);
            $arrayUsuarios[]=$myUsuario;
        }
        return $arrayUsuarios;
    }
}
?>