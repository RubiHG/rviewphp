<?php
include ("conexion.php");

class CrudResenas{
    // constructor de la clase
	public function __construct(){}
		
	function agregarResena($resena){
		$consulta="INSERT INTO resenas (idCategoria, idUsuario, contenido, estrellas, titulo) VALUES (".$resena->getIdCategoria().", ".$resena->getIdUsuario().", '".$resena->getContenido()."','".$resena->getEstrellas()."','".$resena->getTitulo()."')";
		$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
		mysqli_close($GLOBALS['conexion']);
		return $datos;
	}

    
    function editarRese($resena){
        $consulta="UPDATE resenas SET idCategoria='".$resena->getIdCategoria()."',contenido='".$resena->getContenido()."', estrellas='".$resena->getEstrellas()."', titulo='".$resena->getTitulo()."' 
        WHERE idResena=".$resena->getIdResena();
        mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        mysqli_close($GLOBALS['conexion']);
    }

	
    function eliminarRes($resena){
        $consulta="DELETE FROM resenas WHERE idResena=".$resena->getIdResena();
        mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        mysqli_close($GLOBALS['conexion']);
        
    }

	function obtenerTodasResenas($id){
		$arrayResenas =[];
		$consulta="SELECT resenas.*, megusta.idUsuario as megusta FROM resenas LEFT join seguidores on resenas.idUsuario=seguidores.idUsuarioSeguido left JOIN megusta on megusta.idResena= resenas.idResena and megusta.idUsuario=".$id." where resenas.idUsuario=".$id." or seguidores.idUsuario=".$id;
		$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
		while ($fila=mysqli_fetch_array($datos)){
			$myResena= new Resena();
					$myResena->setIdResena($fila['idResena']);
					$myResena->setTitulo($fila['titulo']);
					$myResena->setIdCategoria($fila['idCategoria']);
					$myResena->setIdUsuario($fila['idUsuario']);
					$myResena->setContenido($fila['contenido']);
					$myResena->setEstrellas($fila['estrellas']);
					$myResena->setMeGusta($fila['megusta']);
					$consulta2="SELECT correo FROM usuarios WHERE idUsuario=".$fila['idUsuario'];
					$datosUsuario=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
						if($fila=mysqli_fetch_array($datosUsuario)){
							$myResena->setCorreoUsuario($fila['correo']);
						}
			$arrayResenas[]=$myResena;
		}
		$reversed = array_reverse($arrayResenas);
		
		return $reversed;
	}

	function obtenerResenaPorId($id, $idUsuario){
		$myResena= new Resena();
		$consulta="SELECT resenas.*, megusta.idUsuario as megusta FROM resenas left JOIN megusta on megusta.idResena= resenas.idResena and megusta.idUsuario=".$idUsuario." where resenas.idResena=".$id;
		$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
		if ($fila=mysqli_fetch_array($datos)){
					$myResena->setIdResena($fila['idResena']);
					$myResena->setTitulo($fila['titulo']);
					$myResena->setIdCategoria($fila['idCategoria']);
					$myResena->setIdUsuario($fila['idUsuario']);
					$myResena->setContenido($fila['contenido']);
					$myResena->setEstrellas($fila['estrellas']);
					$myResena->setMeGusta($fila['megusta']);
					$consulta2="SELECT correo FROM usuarios WHERE idUsuario=".$fila['idUsuario'];
					$datosUsuario=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
						if($fila=mysqli_fetch_array($datosUsuario)){
							$myResena->setCorreoUsuario($fila['correo']);
						}
		}
		
		return $myResena;
	}

	function obtenerMisResenas($id, $idUsuario){
		$arrayResenas =[];
		$consulta="SELECT resenas.*, megusta.idUsuario as megusta FROM resenas LEFT JOIN compartir ON resenas.idResena = compartir.idResena left JOIN megusta on megusta.idResena= resenas.idResena  and megusta.idUsuario=".$idUsuario." WHERE resenas.idUsuario =".$id." OR compartir.idUsuario=".$id;
        
		$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
		while ($fila=mysqli_fetch_array($datos)){
			$myResena= new Resena();
					$myResena->setIdResena($fila['idResena']);
					$myResena->setTitulo($fila['titulo']);
					$myResena->setIdCategoria($fila['idCategoria']);
					$myResena->setIdUsuario($fila['idUsuario']);
					$myResena->setContenido($fila['contenido']);
					$myResena->setEstrellas($fila['estrellas']);
					$myResena->setMeGusta($fila['megusta']);
					$consulta2="SELECT correo FROM usuarios WHERE idUsuario=".$fila['idUsuario'];
					$datosUsuario=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
						if($fila=mysqli_fetch_array($datosUsuario)){
							$myResena->setCorreoUsuario($fila['correo']);
						}
					$arrayResenas[]=$myResena;
		}
		$reversed = array_reverse($arrayResenas);
		
		return $reversed;
	}


	function obtenerResenasPorCategoria($idCategoria, $idUsuario){
		$arrayResenas = array();
		$consulta="SELECT resenas.*, megusta.idUsuario as megusta FROM resenas left JOIN megusta on megusta.idResena= resenas.idResena  and megusta.idUsuario=".$idUsuario." WHERE idCategoria=".$idCategoria;
		$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
		while ($fila=mysqli_fetch_array($datos)){
			$myResena= new Resena();
					$myResena->setIdResena($fila['idResena']);
					$myResena->setTitulo($fila['titulo']);
					$myResena->setIdCategoria($fila['idCategoria']);
					$myResena->setIdUsuario($fila['idUsuario']);
					$myResena->setContenido($fila['contenido']);
					$myResena->setEstrellas($fila['estrellas']);
					$myResena->setMeGusta($fila['megusta']);
					$consulta2="SELECT correo FROM usuarios WHERE idUsuario=".$fila['idUsuario'];
					$datosUsuario=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
						if($fila=mysqli_fetch_array($datosUsuario)){
							$myResena->setCorreoUsuario($fila['correo']);
						}
			$arrayResenas[]=$myResena;
		}
		$reversed = array_reverse($arrayResenas);
		return $reversed;
	}

	function obtenerComentariosPorResena($id){
		$arrayComentarios = array();
		$consulta="SELECT * FROM comentarios WHERE idResena=".$id;
		$datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
		while ($fila=mysqli_fetch_array($datos)){
			$myComentario= new Comentario();
					$myComentario->setIdComentario($fila['idComentario']);
					$myComentario->setIdResena($fila['idResena']);
					$myComentario->setDescripcion($fila['descripcion']);
					$myComentario->setFecha($fila['fecha']);
					$myComentario->setIdUsuario($fila['idUsuario']);
					$consulta2="SELECT correo, username FROM usuarios WHERE idUsuario=".$fila['idUsuario'];
					$datosUsuario=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
						if($fila=mysqli_fetch_array($datosUsuario)){
							$myComentario->setCorreo($fila['correo']);
							$myComentario->setUsername($fila['username']);
						}
			$arrayComentarios[]=$myComentario;
		}
		$reversed = array_reverse($arrayComentarios);
		return $reversed;
	}

	function obtenerReseñasMeGustaPorUsuario($idUsuario){
        $arrayResenas = [];
        $consulta="SELECT resenas.*, megusta.idUsuario as megusta FROM resenas INNER JOIN megusta ON megusta.idResena= resenas.idResena WHERE megusta.idUsuario=".$idUsuario;
        $datos=mysqli_query($GLOBALS['conexion'],$consulta) or die (mysqli_error($GLOBALS['conexion']));
        while ($fila=mysqli_fetch_array($datos)){
			$myResena= new Resena();
					$myResena->setIdResena($fila['idResena']);
					$myResena->setTitulo($fila['titulo']);
					$myResena->setIdUsuario($fila['idUsuario']);
					$myResena->setContenido($fila['contenido']);
					$myResena->setEstrellas($fila['estrellas']);
					$myResena->setMeGusta($fila['megusta']);
					$consulta2="SELECT correo FROM usuarios WHERE idUsuario=".$fila['idUsuario'];
					$datosUsuario=mysqli_query($GLOBALS['conexion'],$consulta2) or die (mysqli_error($GLOBALS['conexion']));
						if($fila=mysqli_fetch_array($datosUsuario)){
							$myResena->setCorreoUsuario($fila['correo']);
						}
			$arrayResenas[]=$myResena;
        }
		$reversed = array_reverse($arrayResenas);
        return $reversed;
    }
}
?>