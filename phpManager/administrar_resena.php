<?php
//incluye la clase Libro y CrudLibro
require_once('resenas.php');
require_once('pojo/resena.php');
require_once('pojo/comentario.php');

$crud= new CrudResenas();
$resena= new Resena();
$comentario= new Comentario();
 
	// si el elemento insertar no viene nulo llama al crud y registra un usuario
	if (isset($_POST['publicar'])) {
		$resena->setIdUsuario($_POST['idUsuario']);
        $resena->setContenido($_POST['contenido']);
        $resena->setIdCategoria($_POST['idCategoria']);
        $resena->setEstrellas($_POST['estrellas']);
		$resena->setTitulo($_POST['titulo']);
        
        $myResena=$crud->agregarResena($resena);
		//header('Location: VerUsuarios.php');
		if($myResena=="1"){
			header('Location: ../Index.php?id='.$_POST['idUsuario']);
		}else{
			header('Location: ../login.php');
            echo "$myUsuario->getIdUsuario()";
		}

		//llama a la función insertar definida en el crud
		//$crud->registrar($usuario);
		//header('Location: ../Index.php');
	// si el elemento de la vista con nombre actualizar no viene nulo, llama al crud y actualiza el usuario
	}elseif(isset($_POST['publicarPerfil'])) {
		$resena->setIdUsuario($_POST['idUsuario']);
        $resena->setContenido($_POST['contenido']);
        $resena->setIdCategoria($_POST['idCategoria']);
        $resena->setEstrellas($_POST['estrellas']);
		$resena->setTitulo($_POST['titulo']);
        
        $myResena=$crud->agregarResena($resena);
		//header('Location: VerUsuarios.php');
		if($myResena=="1"){
			header('Location: ../perfil.php?id='.$_POST['idUsuario']."&visitado=".$_POST['visitado']);
		}

		//llama a la función insertar definida en el crud
		//$crud->registrar($usuario);
		//header('Location: ../Index.php');
	// si el elemento de la vista con nombre actualizar no viene nulo, llama al crud y actualiza el usuario
	}elseif(isset($_POST['actualizar'])){
		$usuario->setIdUsuario($_POST['idUsuario']);
		$usuario->setUsername($_POST['username']);
		$usuario->setContrasena($_POST['contrasena']);
		$usuario->setCorreo($_POST['correo']);
		$usuario->setSobremi($_POST['sobremi']);
		$crud->editarUsuario($usuario);
		header('Location: ../perfil.php?id='.$_POST['idUsuario']);
	
	// si la variable accion enviada por GET es == 'ingresar' llama al crud y verifica al usuario
	}
     
    elseif(isset($_POST['actualizarResena'])){
        
		$resena->setIdResena($_POST['idResena']);
        $resena->setContenido($_POST['contenido']);
        $resena->setIdCategoria($_POST['idCategoria']);
        $resena->setEstrellas($_POST['estrellas']);
		$resena->setTitulo($_POST['titulo']);
		$crud->editarRese($resena);
		header('Location: ../perfil.php?id='.$_POST['idUsuario']."&visitado=".$_POST['visitado']);
	
	// si la variable accion enviada por GET es == 'ingresar' llama al crud y verifica al usuario
	}elseif(isset($_POST['eliminarR'])){
		$resena->setIdResena($_POST['idResena']);
		$crud->eliminarRes($resena);
        header('Location: ../perfil.php?id='.$_POST['idUsuario']."&visitado=".$_POST['visitado']);
	// si la variable accion enviada por GET es == 'ingresar' llama al crud y verifica al usuario
	}
?>