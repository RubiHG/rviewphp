<?php
require_once('phpManager/usuarios.php');
require_once('phpManager/pojo/usuario.php');
require_once('phpManager/usuarios.php');
require_once('phpManager/pojo/usuario.php');
$crudUsuario=new CrudUsuario();
$usuario=$crudUsuario->verPerfil($_GET["id"]);
?>
<html lang="en">
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>RView</title>
        <link rel="icon" type="image/png" href="img/Rview.png"/>
        <link type="text/css" href="css/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link type="text/css" href="css/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="css/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
        <link href="css/fontawesome/css/all.min.css" rel="stylesheet" />
        <link href="css/popup.css" rel ="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="css/css-registro/util.css">
        <link rel="stylesheet" type="text/css" href="css/theme.css">
    </head>
    <body>
    <div class="wrapper">
            <header class="header-top" header-theme="light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between">
                        <div class="top-menu d-flex align-items-center">
                            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                            <div class="header-search row" style="width: 500;">
                                <div class="input-group col-md-11">
                                    <a href=<?php echo "Index.php?id=".$_GET["id"]?> class="btn btn-light ml-10"><i class="fa fa-home"></i>Inicio</a>
                                    <a href=<?php echo "categoria.php?id=1&idPerfil=".$_GET["id"]?> class="btn btn-light ml-10"><i class="far fa-bookmark"></i>Categorias</a>                                   
                                </div>
                                <div class="col-md-1">
                                    <img src="img/LogoRview.png" width="90" height="30">
                                </div>
                            </div>
                        </div>
                        <div class="top-menu d-flex align-items-center">
                            <div class="dropdown">
                                <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="img/user.png" alt=""></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href=<?php echo "perfil.php?id=".$_GET["id"]."&visitado=".$_GET["id"]?>><i class="far fa-user"></i> Perfil</a>
                                    <a class="dropdown-item" href="<?php echo "editarPerfil.php?id=".$_GET["id"]?>"><i class="fa fa-cogs"></i> Editar</a>
                                    <a class="dropdown-item" href="login.php"><i class="fa fa-sign-out-alt"></i> Salir</a>
                                </div>
                            </div>
                            <button type="button" class="btn btn-light ml-10 btnresena" data-toggle="modal" data-target="#exampleModalCenter"><i class="far fa-star"></i>Reseña</button>
                        </div>
                    </div>
                </div>
            </header>
            <div class="page-wrap">
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="text-center"> 
                                            <img src="img/user.png" class="rounded-circle" width="150" height="150">
                                            <hr class="mb-0"> 
                                            <h4 class="card-title mt-10"><?php echo $usuario->getUsername()?></h4>
                                            <p class="card-subtitle"><?php echo $usuario->getCorreo()?></p>
                                            <p><?php echo $usuario->getSobremi()?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7">
                            <div class="card">
                                <div class="card-body">
                                    <form  action='phpManager/administrar_usuario.php' method='post'>
                                    <input type='hidden' name='idUsuario' value='<?php echo $_GET["id"]?>'>
                                        <div class="wrap-input100 validate-input" data-validate="Nombre de usuario requerido">
                                            <input class="input100 <?php if($usuario->getUsername()!=null){ echo "has-val";}?>" type="text" name="username" value="<?php echo $usuario->getUsername()?>">
                                            <span class="focus-input100"></span>
                                            <span class="label-input100">Nombre de usuario</span>
                                        </div>  
                                        
                                        <div class="wrap-input100 validate-input" data-validate="Correo requerido">
                                            <input class="input100 <?php if($usuario->getCorreo()!=null){ echo "has-val";}?>" type="text" name="correo" value="<?php echo $usuario->getCorreo()?>">
                                            <span class="focus-input100"></span>
                                            <span class="label-input100">Correo</span>
                                        </div>

                                        <div class="wrap-input100 validate-input" data-validate="Contraseña requerida">
                                            <input class="input100 <?php if($usuario->getContrasena()!=null){ echo "has-val";}?>" type="password" name="contrasena" value="<?php echo $usuario->getContrasena()?>">
                                            <span class="focus-input100"></span>
                                            <span class="label-input100">Contraseña</span>
                                        </div>
                                        
                                        <div class="wrap-input100 validate-input">
                                            <input class="input100 <?php if($usuario->getSobremi()!=null){ echo "has-val";}?>" type="text" name="sobremi" value="<?php echo $usuario->getSobremi()?>">
                                            <span class="focus-input100"></span>
                                            <span class="label-input100">Sobre mí</span>
                                        </div>  
                                        <input type='hidden' name='actualizar' value='actualizar'>
                                        <div class="row ml-4 mr-4">
                                            <div class="col-6">
                                                <div class="container-perfil-form-btn">
                                                    <button class="buttonperfil perfil-form-btn" type="submit" value="Submit">Aceptar</button>
                                                </div> 
                                            </div>
                                            <div class="col-6">
                                                <div class="container-perfil-form-btn">
                                                    <button onclick="location.href='<?php echo 'perfil.php?id='.$_GET['id'].'&visitado='.$_GET['id']?>'" class="buttonperfil perfil-form-btn">Cancelar</button>
                                                </div> 
                                            </div>
                                        </div>
                                    </form>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content overlay active">
                        <div class=" popup active" id="popup">
                            <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup" data-dismiss="modal"><i class="fas fa-times"></i></a>
                            <h3>Danos tu opinion</h3>
                            <form action='phpManager/administrar_resena.php' method='post'>
                            <input type='hidden' name='idUsuario' value=<?php echo $_GET["id"]?>>
                                <div class="contenedor-inputs">
                                    <div class="wrap-input100 validate-input" data-validate="Titulo requerido">
                                        <input class="input100" type="text" name="titulo">
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Titulo</span>
                                    </div>
                                    <div class="wrap-textarea100 validate-input" data-validate="Contenido requerida">
                                        <textarea class="input100"  name="contenido" rows="6" cols="50" ></textarea>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Contenido</span>
                                    </div>
                                    
                                    <div class="wrap-input100 validate-input" data-validate="Estrellas requerida">
                                        <select class="custom-select input100" id="inputGroupSelect01" name="idCategoria">
                                            <option > </option>
                                            <option value="1">Libro</option>
                                            <option value="2">Película</option>
                                            <option value="3">Serie</option>
                                            <option value="4">Música</option>
                                        </select>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Categoría</span>
                                    </div>
                                    <div class="wrap-input100 validate-input" data-validate="Categoria requerida">
                                        <select class="custom-select input100" id="inputGroupSelect0" name="estrellas">
                                            <option > </option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <span class="focus-input100"></span>
                                        <span class="label-input100">Estrellas</span>
                                    </div>
                                </div>
                                <input type='hidden' name='publicar' value='publicar'>
                                <div class="col-6 offset-3 btn" >
                                    <button class="buttonperfil perfil-form-btn" style="margin-top: 0px;" type="submit" value="Submit">Compartir</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
                <footer class="footer">
                    <div class="w-100 clearfix">
                        <span class="text-center text-sm-left d-md-inline-block">Copyright © 2018 uv.mx v1.0. Todos los derechos reservados.</span>
                        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Creado por <a href="#" class="text-dark" target="_blank">Rubix, Rola, Kat, Solis, Andrelolelo, Geral</a></span>
                    </div>
                </footer>
            </div>
        </div>
        <script src="scripts/jquery-3.3.1.js"></script>
        <script src="scripts/popper.min.js"></script>
        <script src=""></script>
        <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/moment.js"></script>
        <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/main.js"></script>
        <script src="css/theme.css"></script>
    </body>
</head>