<?php
if(isset($_GET['centella'])=='true'){
	if($_GET['centella']==1){
		echo "<script> window.onload=function(){alert('Usuario o contraseña incorrectos.')};</script>";  
	}
}
?>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="img/Rview.png"/>
<!--===============================================================================================-->
    
    <link rel="stylesheet" type="text/css" href="css/css-registro/font-awesome.min.css">
<!--===============================================================================================-->
	

	<link rel="stylesheet" type="text/css" href="css/css-registro/util.css">
	<link rel="stylesheet" type="text/css" href="css/css-registro/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action='phpManager/administrar_usuario.php' method='post'>
					<span class="login100-form-title p-b-43">
						Inicia sesión en RView
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Correo valido requerido: ex@abc.xyz">
						<input class="input100" type="text" name="correo" >
						<span class="focus-input100"></span>
						<span class="label-input100">Correo electronico</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Contraseña es requerida">
						<input class="input100" type="password" name="contrasena">
						<span class="focus-input100"></span>
						<span class="label-input100">Contraseña</span>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Inicia sesión
						</button>
					</div>
					<input type='hidden' name='ingresar' value='ingresar'>
					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							<a href="registro.php" type="submit" value="Submit">Registrate</a>
						</span>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('img/bg4.png');">
				</div>
			</div>
		</div>
	</div>
<!--===============================================================================================-->
	<script src="js/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->

	<script src="js/main.js"></script>

</body>
</html>